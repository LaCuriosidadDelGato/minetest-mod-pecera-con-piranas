local S = minetest.get_translator(minetest.get_current_modname())

local function esNodoPez(nodo)
	if type(string.find(nodo, "fish", 1))=="nil" then
		return false
	else
		return true
	end
end

-- espinas
minetest.register_craftitem("pecera_pirana:espinas", {
	description = S("Espinas"),
	inventory_image = "pecera_pirana_espinas.png",
    groups = {bone = 1}
})

-- crea un hueso
minetest.register_craft({
	output = "bonemeal:bone",
	recipe = {
		{"pecera_pirana:espinas", "pecera_pirana:espinas"}
	}
})

-- crea la pecera
minetest.register_craft({
	output = "pecera_pirana:pecera_vacia",
	recipe = {
		{"group:wood", "ethereal:fish_piranha",	"group:wood"},
		{"group:wood", "bucket:bucket_water", "group:wood"},
		{"group:wood", "group:wood", 		"group:wood"}
	}
})

minetest.register_node("pecera_pirana:pecera_vacia", {
	description = S("Pecera sin comida"),
	drawtype = "nodebox",
    tiles = {
          "pecera_pirana_vacia_top.png",    -- TOP
          "default_wood.png",  -- BOT
          "default_wood.png",  -- LEFT
          "default_wood.png",  -- RIGHT
          "default_wood.png",  -- FRONT
          "default_wood.png",  -- BACK
        },

    node_box = {
		type = "fixed",
		fixed = {
			{-0.3750, -0.4375, -0.4375,  0.3750,  0.5000, -0.3125}, --Wände N
			{-0.4375, -0.4375, -0.3750, -0.3125,  0.5000,  0.3750}, --Wände O
			{ 0.3750, -0.4375,  0.4375, -0.3750,  0.5000,  0.3125}, --Wände S
			{ 0.4375, -0.4375,  0.3750,  0.3125,  0.5000, -0.3750}, --Wände W
			{-0.3750, -0.5000, -0.3750,  0.3750,  0.3750,  0.3750}, --Boden
		},
	},

	selection_box = {
		type = "fixed",
		fixed = {
--			{-0.5, -0.5, -0.5, 0.5, 0.5, 0.5}, -- ganze Block
			{-0.3750, -0.4375, -0.4375,  0.3750,  0.5000, -0.3125}, --Wände N
			{-0.4375, -0.4375, -0.3750, -0.3125,  0.5000,  0.3750}, --Wände O
			{ 0.3750, -0.4375,  0.4375, -0.3750,  0.5000,  0.3125}, --Wände S
			{ 0.4375, -0.4375,  0.3750,  0.3125,  0.5000, -0.3750}, --Wände W
			{-0.3750, -0.5000, -0.3750,  0.3750,  0.5000,  0.3750}, --Boden
		}
	},

	groups = {choppy = 2, oddly_breakable_by_hand = 1},

	on_construct = function(pos, node)
        local meta = minetest.get_meta(pos)
        
        meta:set_string("infotext", "Pecera de pirañas")
    end,

    on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
        if clicker:is_player() then
	        local nodename = itemstack:get_name()
			if esNodoPez(nodename) then	
                if not minetest.settings:get_bool("creative_mode") then
                    itemstack:take_item()
                end
                minetest.swap_node(pos, { name = "pecera_pirana:pecera_llena" })
                local timer = minetest.get_node_timer(pos)
                timer:start(10.5) -- in seconds
            else
                minetest.chat_send_player(clicker:get_player_name(), S("¡Necesitas peces!"))
                return itemstack
	        end
        end
    end,
})

minetest.register_node("pecera_pirana:pecera_llena", {
	description = S("Pecera con comida"),
	drawtype = "nodebox",
    tiles = {
          "pecera_pirana_llena_top.png",    -- TOP
          "default_wood.png",  -- BOT
          "default_wood.png",  -- LEFT
          "default_wood.png",  -- RIGHT
          "default_wood.png",  -- FRONT
          "default_wood.png",  -- BACK
        },

	node_box = {
		type = "fixed",
		fixed = {
			{-0.3750, -0.4375, -0.4375,  0.3750,  0.5000, -0.3125}, --Wände N
			{-0.4375, -0.4375, -0.3750, -0.3125,  0.5000,  0.3750}, --Wände O
			{ 0.3750, -0.4375,  0.4375, -0.3750,  0.5000,  0.3125}, --Wände S
			{ 0.4375, -0.4375,  0.3750,  0.3125,  0.5000, -0.3750}, --Wände W
			{-0.3750, -0.5000, -0.3750,  0.3750,  0.3750,  0.3750}, --Boden
		},
	},

	selection_box = {
		type = "fixed",
		fixed = {
--			{-0.5, -0.5, -0.5, 0.5, 0.5, 0.5}, -- ganze Block
			{-0.3750, -0.4375, -0.4375,  0.3750,  0.5000, -0.3125}, --Wände N
			{-0.4375, -0.4375, -0.3750, -0.3125,  0.5000,  0.3750}, --Wände O
			{ 0.3750, -0.4375,  0.4375, -0.3750,  0.5000,  0.3125}, --Wände S
			{ 0.4375, -0.4375,  0.3750,  0.3125,  0.5000, -0.3750}, --Wände W
			{-0.3750, -0.5000, -0.3750,  0.3750,  0.5000,  0.3750}, --Boden
		}
	},

	groups = {choppy = 2, oddly_breakable_by_hand = 1},

   on_construct = function(pos)
		 local timer = minetest.get_node_timer(pos)
		 timer:start(10.5) -- in seconds
   end,

   on_timer = function(pos)
      minetest.set_node(pos, { name = "pecera_pirana:pecera_lista" })
      return false
   end

})

minetest.register_node("pecera_pirana:pecera_lista", {
	description = S("Pecera lista"),
	drawtype = "nodebox",
    tiles = {
          "pecera_pirana_lista_top.png", -- TOP
          "default_wood.png",  -- BOT
          "default_wood.png",  -- LEFT
          "default_wood.png",  -- RIGHT
          "default_wood.png",  -- FRONT
          "default_wood.png",  -- BACK
    },
	node_box = {
		type = "fixed",
		fixed = {
			{-0.3750, -0.4375, -0.4375,  0.3750,  0.5000, -0.3125}, --Wände N
			{-0.4375, -0.4375, -0.3750, -0.3125,  0.5000,  0.3750}, --Wände O
			{ 0.3750, -0.4375,  0.4375, -0.3750,  0.5000,  0.3125}, --Wände S
			{ 0.4375, -0.4375,  0.3750,  0.3125,  0.5000, -0.3750}, --Wände W
			{-0.3750, -0.5000, -0.3750,  0.3750,  0.3750,  0.3750}, --Boden
		},
	},
	groups = {choppy = 2, oddly_breakable_by_hand = 1},

	selection_box = {
		type = "fixed",
		fixed = {
--			{-0.5, -0.5, -0.5, 0.5, 0.5, 0.5}, -- ganze Block
			{-0.3750, -0.4375, -0.4375,  0.3750,  0.5000, -0.3125}, --Wände N
			{-0.4375, -0.4375, -0.3750, -0.3125,  0.5000,  0.3750}, --Wände O
			{ 0.3750, -0.4375,  0.4375, -0.3750,  0.5000,  0.3125}, --Wände S
			{ 0.4375, -0.4375,  0.3750,  0.3125,  0.5000, -0.3750}, --Wände W
			{-0.3750, -0.5000, -0.3750,  0.3750,  0.5000,  0.3750}, --Boden
		}
	},

   on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
      if clicker:is_player() then
         minetest.swap_node(pos, { name = "pecera_pirana:pecera_vacia" })

         local inv = clicker:get_inventory()
         if not inv then
            minetest.add_item(clicker:get_pos(), {name = "default:dirt"})
            return
         end

			if minetest.get_modpath("bonemeal") then
            local remaining = inv:add_item("main", {name = "pecera_pirana:espinas"})
            if not remaining:is_empty() then
               minetest.add_item(clicker:get_pos(), {name = "pecera_pirana:espinas"})
            end
			else
            local remaining = inv:add_item("main", {name = "default:dirt"})
            if not remaining:is_empty() then
               minetest.add_item(clicker:get_pos(), {name = "default:dirt"})
            end
			end

      end
   end,
})

